import axios from 'axios'

const SERVER = 'http://localhost:8080/api'

class ShowStore{
    constructor(ee){
        this.ee=ee
        this.state={
            content:[],
            token:""
        }

        let serializedCredentials=localStorage.getItem("Credentials");
        if(serializedCredentials!=null){
            let credentials=JSON.parse(serializedCredentials);
            this.state.token=credentials.token;
        }

        
    }

    getAll(){
        axios(SERVER + '/shows')
          .then((response) => {
            this.content = response.data
            this.ee.emit('SHOWS_LOAD')
          })
          .catch((error) => console.warn(error))
    }

    getTopShows(){
        axios(SERVER + '/topshows')
         .then((response) => {
             this.content=response.data
             this.ee.emit('TOP_SHOWS_LOAD')
         })
         .catch((error)=> console.warn(error))
    }

    postAddShow(showId, token){
        axios(
            {
                method:"post",
                url:SERVER+"/addshow/"+showId,
                headers:{
                    "Authorization":token
                }
            }
        ).then((response) => {
            this.ee.emit('SHOW_ADDED')
          })
          .catch((error) => console.warn(error))
    }

    getSeenShows(tokenp){
        axios({
                method:"get",
                url:SERVER + '/seenshows',
                headers:{
                    "Authorization":tokenp
                }
        })
        .then((response) => {
            this.content=response.data
            this.ee.emit('SEEN_SHOWS_LOAD')
        })
        .catch((error)=> console.warn(error))

    }
}

export default ShowStore