use sm;

insert into users values(2,'andrei.adi@yahoo.com','Andrei','M','Adi','1234','0766554433','andrei.adi','user');

insert into shows values(1, 'A bipolar CIA operative becomes convinced a prisoner of war has been turned by al-Qaeda and is planning to carry out a terrorist attack on American soil.','https://upload.wikimedia.org/wikipedia/en/f/f7/Homeland_S2_DVD.jpg','Homeland',2011);
insert into shows values(2, 'A high school chemistry teacher diagnosed with inoperable lung cancer turns to manufacturing and selling methamphetamine in order to secure his family future.','https://ia.media-imdb.com/images/M/MV5BZDNhNzhkNDctOTlmOS00NWNmLWEyODQtNWMxM2UzYmJiNGMyXkEyXkFqcGdeQXVyNTMxMjgxMzA@._V1_UY268_CR4,0,182,268_AL_.jpg','Breaking Bad',2008);
insert into shows values(3, 'Ray Donovan, a professional "fixer" for the rich and famous in LA, can make anyone problems disappear except those created by his own family.','https://ia.media-imdb.com/images/M/MV5BMjQxMTkxNDk4MV5BMl5BanBnXkFtZTgwODIzMDAxMzI@._V1_UX182_CR0,0,182,268_AL_.jpg','Ray Donovan',2011);
insert into shows values(4, 'A chronicled look at the criminal exploits of Colombian drug lord Pablo Escobar, as well as the many other drug kingpins who plagued the country through the years.','https://ia.media-imdb.com/images/M/MV5BMTU0ODQ4NDg2OF5BMl5BanBnXkFtZTgwNzczNTE4OTE@._V1_SY1000_CR0,0,674,1000_AL_.jpg','Narcos',2015);
insert into shows values(5, 'Telepathic waitress Sookie Stackhouse encounters a strange new supernatural world when she meets the mysterious Bill, a southern Louisiana gentleman and vampire.','https://ia.media-imdb.com/images/M/MV5BMDVjZDQ2N2MtMzMxYy00ZjliLTg5YjAtNjk1OTUwYjVjNWQ0XkEyXkFqcGdeQXVyNzA5NjUyNjM@._V1_SY1000_CR0,0,667,1000_AL_.jpg','True blood',2008);
insert into shows values(6, 'A woman who moves into an apartment across the hall from two brilliant but socially awkward physicists shows them how little they know about life outside of the laboratory.','https://ia.media-imdb.com/images/M/MV5BY2FmZTY5YTktOWRlYy00NmIyLWE0ZmQtZDg2YjlmMzczZDZiXkEyXkFqcGdeQXVyNjg4NzAyOTA@._V1_SY1000_CR0,0,666,1000_AL_.jpg','The Big Bang Theory',2008);


insert into seen_shows values (1,1);
insert into seen_shows values (1,2);
insert into seen_shows values (1,3);


insert into episodes values (1,'Pilot','1','1',1);
insert into episodes values (2,'Grace','2','1',1);
insert into episodes values (3,'Clean Skin','3','1',1);
insert into episodes values (4,'Semper','4','1',1);
insert into episodes values (5,'Blind Spot','5','1',1);
insert into episodes values (6,'The Good Soldier','6','1',1);

insert into episodes values(7,'Pilot','1','1',2);
insert into episodes values(8,'Cats in the Bag...','1','1',2);

insert into comments values(1,'Best show ever',null,1,1);
insert into comments values(2,'Not so good',null,1,2);

insert into categories values (1,'Romance');
insert into categories values (2,'Comedy');
insert into categories values (3,'Drama');
insert into categories values (4,'Horror');
insert into categories values (5,'Mistery');
insert into categories values (6,'Thriller ');
insert into categories values (7,'Crime ');


insert into category_show values(2,7);
insert into category_show values(2,3);
insert into category_show values(2,6);
insert into category_show values(1,7);
insert into category_show values(1,5);
insert into category_show values(1,6);


insert into roles values('user','user');

insert into reviews values (1,null,null,5,1,1);
insert into reviews values (2,null,null,2,1,2);








