import axios from 'axios'
const SERVER='http://localhost:8080/api'

class NewsStore{ 
    constructor(ee){
        this.ee=ee
        this.state={
            content:[]        }

    }

    /*getNews(){

        let serializedCredentials=localStorage.getItem("Credentials");
        if(serializedCredentials!=null){
            let credentials=JSON.parse(serializedCredentials);
            //this.state.token=credentials.token;
            this.setState({
                token:credentials.token
            }, this.makeCall())
        }
        
    }*/

    getNews(token){

        console.log(token);
        axios( {
            method:"get",
            url:SERVER+'/twitter',
            headers:{
                "Authorization":token
            }
        })
          .then((response) => {
            console.log(token)
            this.content = response.data
            this.ee.emit('NEWS_LOAD')
          })
          .catch((error) => console.warn(error))

    }

/*
    getNews(){
        let serializedCredentials=localStorage.getItem("Credentials");
        if(serializedCredentials!=null){
            let credentials=JSON.parse(serializedCredentials);
            this.state.token=credentials.token;
        }

        axios(
            {
                method:"get",
                url:SERVER+'/twitter',
                headers:{
                    "Authorization":this.state.token
                }
            }
        ).then((response) => {
            this.content = response.data
            this.ee.emit('NEWS_LOAD')
            console.log("raspuns")
          })
          .catch((error) => console.warn(error))
    }*/
}

export default NewsStore;