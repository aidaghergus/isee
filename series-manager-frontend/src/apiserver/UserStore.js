import axios from 'axios';
const SERVER = 'http://localhost:8080/api'

class UserStore{

    constructor(ee){
        this.ee=ee,
        this.state={
            credentials: null

        }
    }

    postLogin(user){
        axios.post(SERVER+'/login',user)
          .then((response) => {
            this.credentials = response.data
            this.ee.emit('LOGIN_LOAD')
          })
          .catch((error) => console.warn(error))
      }

    postSignUp(user){
        axios.post(SERVER+'/signup',user)
          .then((response) => {
            this.ee.emit('SIGNUP_LOAD')
          })
          .catch((error) => console.warn(error))
    }


}

export default UserStore;