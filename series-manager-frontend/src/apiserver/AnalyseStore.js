import axios from 'axios'

const SERVER = 'http://localhost:8080/api'

class AnalyseStore{
    constructor(ee){
        this.ee=ee
        this.state={
            content:[]
        }

    }

    getBest(){
        axios(SERVER + '/analysis/best')
          .then((response) => {
            this.content = response.data
            this.ee.emit('BEST_ANALYSIS')
          })
          .catch((error) => console.warn(error))
    }

    getAnalysis(showIds){
        let text=[...showIds].join(',');
        console.log(text);


        axios(SERVER + '/analysis/'+text)
          .then((response) => {
            this.content = response.data
            this.ee.emit('ANALYSIS_PROCESSED')
          })
          .catch((error) => console.warn(error))

        console.log(SERVER + '/analysis/'+text);

        
    }
}

export default AnalyseStore