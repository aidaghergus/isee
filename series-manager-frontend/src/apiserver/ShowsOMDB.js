import axios from 'axios'

const SERVER = 'http://localhost:8080/api/shows/search/'

class ShowOMDB{
    constructor(ee, search){
        this.ee=ee
        //this.search=search.replace(new RegExp(" ", 'g'), "+");
        this.search=search
        this.state={
            content:[]
        }
    }

    getSearchResult(){
        axios(SERVER + this.search)
          .then((response) => {
            this.content = response.data
            this.ee.emit('SHOW_OMDB_LOAD')
          })
          .catch((error) => console.warn(error))
    }

}

export default ShowOMDB