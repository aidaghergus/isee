import axios from 'axios';
const SERVER = 'http://localhost:8080/api'

class CategoryStore{

    constructor(ee, catego){
        this.ee=ee,
        this.state={
            content: [],
            categories: catego

        }
    }

    getAll(){
        axios(SERVER + '/categories')
          .then((response) => {
            this.content = response.data
            this.ee.emit('CATEGORY_LOAD')
          })
          .catch((error) => console.warn(error))
      }

    getSeriesByCategories(){
        axios(SERVER + '/categories/search/' + this.state.categories)
          .then((response) => {
            this.content = response.data
            this.ee.emit('CATEGORY_FILTER_LOAD')
          })
          .catch((error) => console.warn(error))
    }

}

export default CategoryStore;