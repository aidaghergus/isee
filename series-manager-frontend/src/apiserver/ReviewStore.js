import axios from 'axios';
const SERVER = 'http://localhost:8080/api'

class ReviewStore{

    constructor(ee){
        this.ee=ee,
        this.state={
            content: []
        }
    }

    getUserReviews(token){
        axios(
            {
                method:"get",
                url:SERVER + '/user/reviews',
                headers:{
                    "Authorization":token
                }
            })
          .then((response) => {
            this.content = response.data
            this.ee.emit('USER_REVIEWS_LOAD')
          })
          .catch((error) => console.warn(error))
      }

      getShowReviews(showId){
        axios(SERVER +'/'+ showId+'/reviews')
          .then((response) => {
            this.content = response.data
            this.ee.emit('SHOW_REVIEWS_LOAD')
          })
          .catch((error) => console.warn(error))
      }

      postReview(token,showId,review){
        axios({
            method:"post",
            url:SERVER+'/'+showId+'/addreview',
            headers:{
              "Authorization":token
          },
              data:review
        }).then((response) => {
            this.ee.emit('REVIEW_ADDED')
          })
          .catch((error) => console.warn(error))
    }

   

}

export default ReviewStore;