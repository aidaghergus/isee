import axios from 'axios';
const SERVER = 'http://localhost:8080/api'

class CommentStore{

    constructor(ee){
        this.ee=ee,
        this.state={
            content: []
        }
    }

    getUserComments(token){
        axios(
            {
                method:"get",
                url:SERVER + '/user/comments',
                headers:{
                    "Authorization":token
                }
            })
          .then((response) => {
            this.content = response.data
            this.ee.emit('USER_COMMENTS_LOAD')
          })
          .catch((error) => console.warn(error))
      }

      getShowComments(showId){
          axios(SERVER+'/'+showId+'/comments')
          .then((response) => {
            this.content = response.data
            this.ee.emit('SHOW_COMMENTS_LOAD')
          })
          .catch((error) => console.warn(error))
      }

      postComment(token,showId,comment){
          axios({
              method:"post",
              url:SERVER+'/'+showId+'/addcomment',
              headers:{
                "Authorization":token
            },
                data:comment
          }).then((response) => {
            this.ee.emit('COMMENT_ADDED')
          })
          .catch((error) => console.warn(error))
      }

   

}

export default CommentStore;