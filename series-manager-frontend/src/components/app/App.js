import React, { Component } from 'react';
import './App.css';
import Login from '../login/Login';
import {Switch, Route} from 'react-router-dom'
import Workspace from '../workspace/Workspace';
import Series from '../series/Series';
import Profile from '../profile/Profile';
import News from '../news/News';
import Analysis from '../analysis/Analysis'
import Navbar from '../workspace/Navbar';

class App extends Component {
  render() {
    return (
      <div className="text-center">
      <Switch>
        <Route path="/login" exact component={Login}/>
        <Route path="/workspace" exact component={Workspace}/>
        <Route path="/series" exact component={Series}/>
        <Route path="/profile" exact component={Profile}/>
        <Route path="/news" exact component={News}/>
        <Route path="/navbar" exact component={Navbar}/>
        <Route path="/analysis" exact component={Analysis}/>
      </Switch>
      </div>
    );
  }
}

export default App;
