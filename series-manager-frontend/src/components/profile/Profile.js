import React, {Component} from 'react';
import Navbar from '../workspace/Navbar';
import './Profile.css'
import SerieItem from '../series/SerieItem';
import SerieItemSimplified from '../series/SeriesItemSimplified'
import {EventEmitter} from 'fbemitter'
import ShowStore from '../../apiserver/ShowStore';
import CommentStore from '../../apiserver/CommentStore';
import ReviewStore from '../../apiserver/ReviewStore';



const ee = new EventEmitter()
const showStore=new ShowStore(ee);
const commentStore=new CommentStore(ee);
const reviewStore=new ReviewStore(ee);


function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

function OptionFct(props){
    if(props.seenSeries==true){
        if(isEmpty(props.seenContent))
            return(<div>You don't have any seen series</div>)
        else{
            return (<div className="row">
                {
                    props.seenContent.map(serie => {
                        return(<SerieItemSimplified key={serie.id}  
                                serieId={serie.id}
                                serieName={serie.name} 
                                serieDescription={serie.description} 
                                serieYear={serie.year} 
                                serieImage={serie.image}
                                serieCategories={serie.categoryList}
                                serieComments={serie.commentList}
                                seriesReviews={serie.reviewList}
                                serieImdbRating={serie.imdbRating}
                                serieImdbVotes={serie.imdbVotes}
                                serieExtRating={serie.externalRating}
                                serieActors={serie.actors}
                                serieAwards={serie.awards}
                                serieSearches={serie.searches} 
                                /> )})
                }
            </div>)
        }
    }
    else if(props.activity==true){
        return (
        <div className="row">
        
                <div className="col-md-10 col-md-offset-1">
                    <div className="panel act-content">
                    {
                        props.comments.map(comment=>{
                            return(<div className="recent-act">
                        
                    
                            <div className="activity-icon terques">
                                <i className="fa fa-comments-o"></i>
                            </div>
                            <div className="activity-desk">
                                <h2>{comment.commentDate}</h2>
                                <p>{comment.user.username} commented on {comment.show.name}: {comment.content}</p>
                            </div>
                            
                                     
                        </div>)
                        })
                        
                    }
                    </div>
                    <div className="panel act-content">
                    {
                        props.reviews.map(review=>{
                            return(<div className="recent-act">
                        
                    
                            <div className="activity-icon terques">
                                <i className="fa fa-star"></i>
                            </div>
                            <div className="activity-desk">
                                <h2>{review.reviewDate}</h2>
                                <p>{review.user.username} reviewed {review.show.name} with {review.stars} - {review.comment}</p>
                            </div>
                            
                                     
                        </div>)
                        })
                        
                    }
                    </div>
                </div>
        
    </div>
        )
    }
    else{
        return <div>Aida</div>
    }
}


class Profile extends Component{
    constructor(){
        super();
        this.state={
            user:null,
            seenSeries:false,
            activity:true,
            seenContent:[],
            activityContent:[],
            token:"",
            userComments:[],
            loggedOn:false,
            userReviews:[]
        }
      

        this.handleActivities=this.handleActivities.bind(this);
        this.handleSeenSeries=this.handleSeenSeries.bind(this); 
    }

    componentDidMount(){
        ee.addListener('SEEN_SHOWS_LOAD', ()=>{
            this.setState({
                seenContent: showStore.content,
                seenSeries:true,
                activity:false
            })
        })
  

        this.setState({
            seenSeries:false,
            activity:false
        })

        let serializedCredentials=localStorage.getItem("Credentials");
        if(serializedCredentials!=null){
            let credentials=JSON.parse(serializedCredentials);
            this.setState({
                token:credentials.token,
                user:credentials.user,
                loggedOn:true
            })
        }


        ee.addListener('USER_COMMENTS_LOAD', ()=>{
            this.setState({
                userComments: commentStore.content,
                seenSeries:false,
                activity:true

            })
        })

        ee.addListener('USER_REVIEWS_LOAD', ()=>{
            this.setState({
                userReviews: reviewStore.content,
                seenSeries:false,
                activity:true

            })
        })
    }

    handleSeenSeries(){
        showStore.getSeenShows(this.state.token);

    }

    handleActivities(){
        commentStore.getUserComments(this.state.token);
        reviewStore.getUserReviews(this.state.token);

    }

    render(){
        if(this.state.loggedOn==false){
            return(<div>You must be logged on</div>)
        }
        else{
        return (
            <div className="main-div-profile">
                <Navbar/>
                    <div className="row">
                            <br/>
                            <br/>

                            <div className="col-lg-4 col-md-4"> 


                                <div className="card">
                                <img className="card-img-top" src="https://www.weact.org/wp-content/uploads/2016/10/Blank-profile.png" alt="https://www.weact.org/wp-content/uploads/2016/10/Blank-profile.png"/>
                                <div className="card-body">
                                    <h5 className="card-title">{this.state.user.firstName + " "+ this.state.user.lastName}</h5>
                                    <p className="card-text">{"Username: "+this.state.user.username}</p>
                                    <p className="card-text">{"Email: "+this.state.user.email}</p>
                                    <p className="card-text">{"Phone: "+ this.state.user.phone}</p>

                                    <a className="btn btn-primary" onClick={this.handleSeenSeries}>Seen series</a><br/>
                                    <a className="btn btn-primary" onClick={this.handleActivities}>Activity</a>
                                </div>
                                </div>
                            </div>

                            <div className="col-lg-8 col-md-8"> 
                                <OptionFct seenSeries={this.state.seenSeries} activity={this.state.activity} theUser={this.state.user} seenContent={this.state.seenContent} comments={this.state.userComments} reviews={this.state.userReviews}/>
                            </div>
                    </div>

            </div>
        )
    }
    }
}

export default Profile;