import React, {Component} from 'react'
import Navbar from '../workspace/Navbar';
import './Analysis.css'
import AnalyseStore from '../../apiserver/AnalyseStore';
import {EventEmitter} from 'fbemitter'
import {Animated} from "react-animated-css";


const ee = new EventEmitter()
const analysisStore=new AnalyseStore(ee)


class Analysis extends Component{

    constructor(props){
        super(props);
        this.state ={
            analysedShows:[],
            loading:true,
            bestAnalysis: []
        }
    }

    componentDidMount(){

        analysisStore.getAnalysis(this.props.location.state.detail);
        ee.addListener('ANALYSIS_PROCESSED', ()=>{
            this.setState({
                analysedShows: analysisStore.content,
                loading:false
            }, analysisStore.getBest())
        });

        
        ee.addListener('BEST_ANALYSIS', ()=>{
            this.setState({
                bestAnalysis: analysisStore.content
            })
        });
    
    }

    render(){
        if(this.state.loading){
            return (
                <div>
                <Navbar/>
                <div className="main-div">
                <div className="container">
                    <div className="row">
                        <div id="loader">
                            <div className="dot"></div>
                            <div className="dot"></div>
                            <div className="dot"></div>
                            <div className="dot"></div>
                            <div className="dot"></div>
                            <div className="dot"></div>
                            <div className="dot"></div>
                            <div className="dot"></div>
                            <div className="lading"></div>
                        </div>
                    </div>
                    <br/>
                    <br/>                  
                    <br/>
                    <br/>
                    <br/>
                    <p className="text-primary">Analysing...</p>
                </div>
                </div>
                </div>
            )

        }
        else{
        return(
            
            <div className="container">
                <div className="row"><Navbar/></div>
                <div id="tabel" className="row">
                <table className="table table-bordered">
                <thead>
                    <tr>
                    <th scope="col">Show</th>
                    <th scope="col">Text length</th>
                    <th scope="col">Html length</th>
                    <th scope="col">Out links</th>
                    <th scope="col">Sentiment tones</th>
                    <th scope="col">Sentiment score</th>
                    <th scope="col">Search level</th>
                    <th scope="col">Imdb Rating</th>
                    <th scope="col">Imdb Votes</th>
                    <th scope="col">External Rating</th>


                    </tr>
                </thead>
                <tbody>
                    {
                        this.state.analysedShows.map(analysis =>{
                            return(
                                <tr key={analysis.show_id}>
                                <th scope="row">{analysis.show_name}</th>
                                <td>{analysis.textLengthAvg}</td>
                                <td>{analysis.htmlLengthAvg}</td>
                                <td>{analysis.outLinksAvg}</td>
                                <td>{analysis.sentimentTones}</td>
                                <td>{analysis.sentimentScore}</td>
                                <td>{analysis.searchesLevel}</td>
                                <td>{analysis.imdbRating}</td>
                                <td>{analysis.imdbVotes}</td>
                                <td>{analysis.externalRating}</td>
                                </tr>
                            )

                        })
                    }
                   
                 
                </tbody>
                </table>

                <div className="container">

                <Animated animationIn="tada" animationOut="fadeOutRight" isVisible={true}>
                    <div className="the-winner">
                        <h3>The winner is:</h3>
                    </div>
                </Animated>

                <figure className="figure">
                    <img src={this.state.bestAnalysis.image}  className="figure-img img-fluid rounded" alt="A generic square placeholder image with rounded corners in a figure."/>
                    <figcaption className="figure-caption">{this.state.bestAnalysis.name}</figcaption>
                </figure>
                </div>

                </div>
                <div>
                    </div>
            </div>
        )
    }
    }
}

export default Analysis;