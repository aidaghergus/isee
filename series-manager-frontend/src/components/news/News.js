import React, {Component} from 'react';
import Navbar from '../workspace/Navbar';
import {EventEmitter} from 'fbemitter'
import NewsStore from '../../apiserver/NewsStore';
import TweetEmbed from 'react-tweet-embed'
import './News.css'


const ee=new EventEmitter();
const newsStore=new NewsStore(ee)

class News extends Component{

    constructor(){
        super()
        this.state={
            news:[],
            loading:true,
            token:""
        }
    }


    componentDidMount(){
        let serializedCredentials=localStorage.getItem("Credentials");
        if(serializedCredentials!=null){
            let credentials=JSON.parse(serializedCredentials);
            this.setState({
                token:credentials.token
            }, newsStore.getNews(credentials.token))
        }

        
        ee.addListener('NEWS_LOAD', () => {

            console.log("intra")

          this.setState({
            news: newsStore.content,
            loading:false
          }, console.log("Updatat state"))
        })
    }

    render(){

        if(this.state.loading){
            return (
                <div>
                <Navbar/>
                <div className="main-div">
                <div className="container">
                    <div className="row">
                        <div id="loader">
                            <div className="dot"></div>
                            <div className="dot"></div>
                            <div className="dot"></div>
                            <div className="dot"></div>
                            <div className="dot"></div>
                            <div className="dot"></div>
                            <div className="dot"></div>
                            <div className="dot"></div>
                            <div className="lading"></div>
                        </div>
                    </div>
                    <br/>
                    <br/>                  
                    <br/>
                    <br/>
                    <br/>
                    <p className="text-primary">Getting news...</p>
                </div>
                </div>
                </div>
            )

        }
        else if(Object.keys(this.state.news).length===0){
            return (
                <div>
                <Navbar/>
                <div className="main-div-news"><br/><br/>
                <h1>You don't have any seen series</h1></div>
                </div>
            )
        }
        else{
        return (
            <div>
                <Navbar/>
                <main role="main" className="container">
                    <div className="main-div-news">
                        {
                            this.state.news.map(tweet => {
                                return (
                                    <TweetEmbed key={tweet.id} id={tweet.idStr} options={{cards: 'hidden' }}/>
                                )
                            })
                        }  
                    </div>
                </main>
            </div>
        )
    }
}
}

export default News;