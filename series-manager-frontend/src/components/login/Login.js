import React, {Component} from 'react';
import './Login.css'
import {EventEmitter} from 'fbemitter'
import UserStore from '../../apiserver/UserStore';


const ee = new EventEmitter()
const userStore = new UserStore(ee)

class Login extends Component{

    constructor(props) {
        super(props);
        this.state = {
            username:"",
            password:"",
            email:"",
            firstname:"",
            lastname:"",
            phone:"",
            loginpressed:""
        }

        this.logIn=this.logIn.bind(this);
        this.handleInputChange=this.handleInputChange.bind(this);
        this.performSignUp=this.performSignUp.bind(this);
    }

    logIn(){
        let pbody={
            username: this.state.username,
            password:this.state.password
        }
        console.log('button clicked');
        userStore.postLogin(pbody);
        this.setState({
            loginpressed:"Invalid credentials"
        })
        
    }

    componentDidMount(){
        ee.addListener('LOGIN_LOAD', () => {
            let credentials=JSON.stringify(userStore.credentials);
            localStorage.setItem("Credentials",credentials);
            this.props.history.push("/workspace");
          })

          ee.addListener('SIGNUP_LOAD', () => {
            let pbody={
                username: this.state.username,
                password:this.state.password
            }
            userStore.postLogin(pbody);
          })
        
    }

    handleInputChange(e){
        let name=e.target.name;
        let value=e.target.value;
        this.setState({
            [name]:value
        })

    }


    performSignUp(){
        let pbody={
            role:"user",
	        firstName:this.state.firstname,
            lastName:this.state.lastname,
            email:this.state.email,
            phone:this.state.phone,
            gender:'N',
            username:this.state.username,
            password:this.state.password,
            shows:[]
        }

        console.log(pbody);

       userStore.postSignUp(pbody);

    }

    render(){
        return (
            <div>
            <div className="main-div">
                <form className="form-login">
                    <img className="mb-4" src="https://thumbs.dreamstime.com/b/pantera-blue-eyes-colorful-illustration-graphic-background-59986293.jpg" alt="" width="72" height="72"/>
                    <h1 className="h3 mb-3 font-weight-normal">Please log in</h1>
                    <label htmlFor="inputEmail" className="sr-only">Email address</label>
                    <input name="username" type="text" id="username" value={this.state.username} className="form-control" placeholder="Email address" required onChange={this.handleInputChange}/>
                    <label htmlFor="inputPassword" className="sr-only">Password</label>
                    <input name="password" type="password" id="password" value={this.state.password} className="form-control" placeholder="Password" required onChange={this.handleInputChange}/>
                
                

                    <p className="text-danger">{this.state.loginpressed}</p>
                    <button className="btn btn-lg btn-primary btn-block" type="button" onClick={this.logIn}>Log in</button>


                    <button type="button" className="btn btn-outline-primary" data-toggle="modal" data-target="#signup">or sign up</button>

                    <p className="mt-5 mb-3 text-muted">&copy; 2018</p>
                </form>
        



            <div className="modal fade" id="signup" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                <div className="modal-header">
                    
                    
                </div>
                <div className="modal-body">
                <div className="container">
            <h1>Sign Up</h1>
            <p>Please fill in this form to create an account.</p>
            <hr/>

            <label><b>First name</b></label>
            <input type="text" placeholder="Enter Firstname" name="firstname" required onChange={this.handleInputChange}/>

            <label><b>Last name</b></label>
            <input type="text" placeholder="Enter Lastname" name="lastname" required onChange={this.handleInputChange}/>

            <label><b>Email</b></label>
            <input type="text" placeholder="Enter Email" name="email" required onChange={this.handleInputChange}/>

            <label><b>Username</b></label>
            <input type="text" placeholder="Enter Username" name="username" required onChange={this.handleInputChange}/>

            <label><b>Phone</b></label>
            <input type="text" placeholder="Enter Phone" name="phone" required onChange={this.handleInputChange}/>

            <label><b>Password</b></label>
            <input type="password" placeholder="Enter Password" name="password" required onChange={this.handleInputChange}/>

            <div className="clearfix">
                <button type="submit" className="signup" onClick={this.performSignUp}>Sign Up</button>
            </div>
            </div>
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                </div>
            </div>
            </div>
            </div>
            </div>
        )
    }
}

export default Login;