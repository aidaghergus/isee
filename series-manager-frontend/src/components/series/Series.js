import React, {Component} from 'react'
import Navbar from '../workspace/Navbar';
import db from '../../apiserver/db'
import Category from './Category';
import SerieItem from './SerieItem';
import CategoryStore from '../../apiserver/CategoryStore';
import {EventEmitter} from 'fbemitter'
import ShowStore from '../../apiserver/ShowStore';
import ShowOMDB from '../../apiserver/ShowsOMDB';
import {Checkbox, CheckboxGroup} from 'react-checkbox-group';
import AnalyseStore from '../../apiserver/AnalyseStore';
import './Series.css'


const ee = new EventEmitter()
const categStore = new CategoryStore(ee,[])
const showStore = new ShowStore(ee)
const updateNavbarEe = new EventEmitter()


function Fct(props){
    if(props.searchActive===true && props.searchResult!=null){
        return <SerieItem  
        serieId={props.searchResult.id}
        serieName={props.searchResult.name} 
        serieDescription={props.searchResult.description} 
        serieYear={props.searchResult.year} 
        serieImage={props.searchResult.image}
        serieCategories={props.searchResult.categoryList}
        serieComments={props.searchResult.commentList}
        seriesReviews={props.searchResult.reviewList}
        serieImdbRating={props.searchResult.imdbRating}
        serieImdbVotes={props.searchResult.imdbVotes}
        serieExtRating={props.searchResult.externalRating}
        serieActors={props.searchResult.actors}
        serieAwards={props.searchResult.awards}
        serieSearches={props.searchResult.searches}
        ee={updateNavbarEe}
        onAddAnalyse={props.fct}

        />
    }
    return null; 
}

Array.prototype.remByVal = function(val) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] === val) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
}

class Series extends Component{

    constructor(){
        super();
        this.state={
            categories: [],
            series: [],
            query: '',
            searchResult:[],
            searchActive: false,
            activeView:[],
            checkedCategories: [],
            seriesToAnalyse: new Set(),
            token:""
        }
        this.searchSerie = this.searchSerie.bind(this);
        this.handleInputChange=this.handleInputChange.bind(this);
        this.handleCheckboxChange=this.handleCheckboxChange.bind(this);
        this.handleAddToAnalyse=this.handleAddToAnalyse.bind(this);
        this.handleLoadingPreview=this.handleLoadingPreview.bind(this);


    }

    componentDidMount(){
        categStore.getAll()
        ee.addListener('CATEGORY_LOAD', () => {
          this.setState({
            categories : categStore.content
          })
        })

        showStore.getAll()
        ee.addListener('SHOWS_LOAD', ()=>{
            this.setState({
                series: showStore.content,
                searchActive: false,
                activeView: showStore.content
            })
        })

      

    }

    searchSerie(e){
        var showOMDBStore=new ShowOMDB(ee, this.state.query);
        showOMDBStore.getSearchResult()
        ee.addListener('SHOW_OMDB_LOAD', ()=>{
            this.setState({
                searchResult: showOMDBStore.content,
                searchActive: true,
                activeView: []
            })
        })
    }

    handleAddToAnalyse (serieToAnalyse) {
        let newseries=this.state.seriesToAnalyse.add(serieToAnalyse);
        this.setState({seriesToAnalyse: newseries});
    }

    handleInputChange = () => {
        this.setState({
          query: this.search.value
        })
        if(this.state.query!='')
            this.setState({
                searchActive: false,
                searchResult: '',
                activeView: this.state.series
            })
        }

    loadGroupsFromQuery(){
        if(this.state.checkedCategories.length>0){
            const filter=new CategoryStore(ee,this.state.checkedCategories);
            filter.getSeriesByCategories()
            ee.addListener('CATEGORY_FILTER_LOAD', () => {
            this.setState({
                activeView : filter.content
            })
        })}
        if(this.state.checkedCategories.length==0){
        
            this.setState({
                activeView : this.state.series
            })
        }
        
    }

    handleCheckboxChange =(isChecked, category) =>{
        if(isChecked==true){
            this.setState({
                checkedCategories: this.state.checkedCategories.concat(category)
            },function() {
                this.loadGroupsFromQuery();
              })
        }
        else{
            this.setState({
                checkedCategories: this.state.checkedCategories.remByVal(category)
            },function() {
                this.loadGroupsFromQuery();
              })
        }
    }

    handleLoadingPreview(){
        this.props.history.push({
            pathname: '/analysis',
            state: { detail: this.state.seriesToAnalyse }
          });
    }

    

    render(){

        return (
            <div>
                <Navbar ee={updateNavbarEe} hist={this.props.history} seriesAnalyse={this.state.seriesToAnalyse} onLoading={this.handleLoadingPreview}/>
                <div className="main-div-series">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="grid search">
                                <div className="grid-body">
                                    <div className="row">
                                        <div className="col-md-3">
                                            <h2 className="grid-title"><i className="fa fa-filter"></i> Filters</h2>
                                            <hr/>
                                            
                                            <h4>By category:</h4>
                                            <ul className="list-group">
                                            {
                                                this.state.categories.map((category, index) => {
                                                return (
                                                    <li className="list-group-item" key={index}>
                                                    <Category id={index} name={category.name} handleChange={this.handleCheckboxChange}/>
                                                    </li>
                                                )})
                                            }
                                            </ul>
                                                                                                                        
                                        </div>
                                     
                                        <div className="col-md-9">
                                            <h2><i className="fa fa-file-o"></i> Result</h2>
                                            <hr/>
                                            <div className="input-group">
                                                <input type="textt" className="form-control" placeholder="Search for..." ref={input => this.search = input} onChange={this.handleInputChange}/>
                                                <span className="input-group-btn">
                                                    <button className="btn btn-primary" type="button" height="100" onClick={this.searchSerie}><i className="fa fa-search">Search</i></button>
                                                </span>
                                            </div>
                                                <br/>                                            
                                            <div className="padding"></div>

                                            <div className="row">
                                                <Fct searchActive={this.state.searchActive} searchResult={this.state.searchResult} series={this.state.series} fct={this.handleAddToAnalyse}/>
                                            </div>
                                            <div className="row">
                                                {
                                                     this.state.activeView.map(function(serie, id) {
                                                        return (
                                                            <SerieItem key={id}  
                                                            serieId={serie.id}
                                                            serieName={serie.name} 
                                                            serieDescription={serie.description} 
                                                            serieYear={serie.year} 
                                                            serieImage={serie.image}
                                                            serieCategories={serie.categoryList}
                                                            serieComments={serie.commentList}
                                                            seriesReviews={serie.reviewList}
                                                            serieImdbRating={serie.imdbRating}
                                                            serieImdbVotes={serie.imdbVotes}
                                                            serieExtRating={serie.externalRating}
                                                            serieActors={serie.actors}
                                                            serieAwards={serie.awards}
                                                            serieSearches={serie.searches} 
                                                            ee={updateNavbarEe}
                                                            onAddAnalyse={this.handleAddToAnalyse}
                                                            />
                                                        )
                                                    }, this)
                                                }
                                            </div>


                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default Series;