import React, {Component} from 'react';
import './SerieItem.css';
import {EventEmitter} from 'fbemitter'
import ShowStore from '../../apiserver/ShowStore';
import toastr from 'toastr';

const ee = new EventEmitter()
const showStore = new ShowStore(ee)

class SerieItem extends Component{

    constructor(props){
        super(props)
        this.state={
            id: props.serieId
        }
    }


    render(){

        return (
                <div className="col-md-4">
                    <div className="card mb-4 box-shadow">
                        <img className="card-img-top" src={this.props.serieImage} alt="?" height="400px"/>
                        <div className="card-body">
                            <p className="card-text"><b>{this.props.serieName}</b></p>
                            <p className="card-text">{this.props.serieDescription}</p>
                            <button type="button" className="btn btn-primary" data-toggle="modal" data-target={"#".concat(this.props.serieId)}>See more</button>
                            <div className="modal fade" id={this.props.serieId} tabIndex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                            <div className="modal-dialog" role="document">
                                <div className="modal-content">
                                <div className="modal-header">

                                    <div className="col-md-8">
                                        <h5 className="modal-title" id="exampleModalLongTitle">{this.props.serieName}</h5>
                                    </div>
                                    <div className="col-md-3 col-md-offset-3" align="right">
                                        <div align="right"><span>{this.props.serieImdbRating}<img src="http://hanslodge.com/images/kcMKBByXi.jpg" height="25" width="25"/></span></div>

                                    </div>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                    </button>
                                    
                                    
                                </div>
                                <div className="modal-body">
                                    <div className="border border-primary">{this.props.serieDescription}</div>
                                    <div align="left"><b>Categories: </b>
                                        {
                                            this.props.serieCategories.map((category, index) => {
                                                return (
                                                    <span key={index}>{category.name}, </span>
                                                )})}
                                    </div>
                                    <div align="left"><b>Year: </b>{this.props.serieYear}</div>
                                    <div align="left"><b>Actors: </b>{this.props.serieActors}</div>
                                    <div align="left"><b>Awards: </b>{this.props.serieAwards}</div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
        )
    }
}

export default SerieItem;