import React from 'react'
//props.handleChange
class Category extends React.Component  {

    constructor(props){
        super(props)
        this.state={
            isChecked:true
        }
        this.onCheckboxChange=this.onCheckboxChange.bind(this)
    }

    onCheckboxChange(event) {
        this.setState({
            isChecked: !this.state.isChecked
        })
        console.log(this.state.isChecked);
        const fieldName = event.target.attributes.name.value;
        console.log(event.target.attributes.name.value);
        this.props.handleChange(this.state.isChecked, fieldName)

    }

    render(){
        return (
            <div className="checkbox">
                <label><input name={this.props.name} type="checkbox" className="icheck" onChange={this.onCheckboxChange}/>{this.props.name}</label>
            </div>
        );
    }

}

export default Category;