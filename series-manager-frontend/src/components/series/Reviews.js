import React, {Component} from 'react'
import Navbar from '../workspace/Navbar';
import StarRatingComponent from 'react-star-rating-component';
import {EventEmitter} from 'fbemitter'
import ReviewStore from '../../apiserver/ReviewStore';

const ee = new EventEmitter()
const reviewStore=new ReviewStore(ee)

class Reviews extends Component{

    constructor(props){
        super(props);
        this.state={
            rating:0,
            commentForReview:"",
            token:""
        }

        this.handleInputChange=this.handleInputChange.bind(this);
        this.addReview=this.addReview.bind(this);
        this.onStarClick=this.onStarClick.bind(this);

    }

    componentDidMount(){
        let serializedCredentials=localStorage.getItem("Credentials");
        if(serializedCredentials!=null){
            let credentials=JSON.parse(serializedCredentials);
            this.setState({
                token:credentials.token
            })
        }

        ee.addListener('REVIEW_ADDED', ()=>{  
            this.setState({
                commentForReview:"",
                rating:0
            })
            this.props.onReviewAdded();

        })
    }

    addReview(){
        let obj={
            id:1,
            stars:this.state.rating,
            reviewDate:"",
            comment:this.state.commentForReview,
            user:{},
            show:{}
        }

        reviewStore.postReview(this.state.token,this.props.showId,obj)

    }

    handleInputChange(e){
        this.setState({
            commentForReview:e.target.value
        })
    }

    onStarClick(nextValue, prevValue, name) {
        this.setState({rating: nextValue});
        console.log("star click")
    }

    render(){
        if(this.props.see==true){
            return (
               
                                            <div className="actionBox">
                                                <ul className="commentList">
                                                   {
                                                       this.props.content.map(review=>{
                                                           return(
                                                                <li key={review.id} className="comment-container">
                                                                    <div className="commenterImage">
                                                                    <img src="https://www.weact.org/wp-content/uploads/2016/10/Blank-profile.png" />
                                                                    </div>
                                                                    <p className="float-left">{review.user.username}</p>
                                                                    <StarRatingComponent 
                                                                    name="rate1" 
                                                                    starCount={5}
                                                                    value={review.stars}
                                                                    />
                                                                    <div className="commentText">
                                                                        <p className="">{review.comment}</p> <span className="date sub-text">on {review.reviewDate}</span>
                
                                                                    </div>
                                                                </li>
                                                           )
                                                       })
                                                   }
                                                </ul>
                                                <div className="form-group-review">
                                               
                                                    <div className="rows">
                                                        <StarRatingComponent 
                                                            name="rate1" 
                                                            starCount={5}
                                                            value={this.state.rating}
                                                            onStarClick={this.onStarClick}
                                                            
                                                            />

                                                             <input className="form-control" type="text" placeholder="Add a comment" value={this.state.commentForReview} onChange={this.handleInputChange}
                                                        />
                                                        <button className="btn btn-outline-primary" onClick={this.addReview}>Add</button>

                                                    </div>
                                                       
                                                    </div>
                                                    
                    
                                            </div>
    
            )}
            else{
                return(<div></div>)
            }
       
    }

}

export default Reviews;

