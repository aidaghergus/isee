import React, {Component} from 'react';
import './SerieItem.css';
import {EventEmitter} from 'fbemitter'
import ShowStore from '../../apiserver/ShowStore';
import toastr from 'toastr';
import Reviews from './Reviews'
import Comments from './Comments';
import ReviewStore from '../../apiserver/ReviewStore';
import CommentStore from '../../apiserver/CommentStore';


const ee = new EventEmitter()
const showStore = new ShowStore(ee)
const reviewStore=new ReviewStore(ee)
const commentStore=new CommentStore(ee)


class SerieItem extends Component{

    constructor(props){
        super(props)
        this.addSerie=this.addSerie.bind(this);
        this.state={
            id: props.serieId,
            rating:0,
            token:"",
            seeReviews:false,
            seeComments:false,
            reviewContent:[],
            commentContent:[]
        }
        this.addToAnalyse=this.addToAnalyse.bind(this)
        this.displayReviews=this.displayReviews.bind(this)
        this.displayComments=this.displayComments.bind(this)
        this.handleOnReviewAdded=this.handleOnReviewAdded.bind(this);
        this.handleOnCommentAdded=this.handleOnCommentAdded.bind(this);
    }

    componentDidMount(){
        ee.addListener('SHOW_ADDED', ()=>{  
      

        })

        ee.addListener('SHOW_REVIEWS_LOAD', ()=>{
            this.setState({
                reviewContent: reviewStore.content
            })
        })

        ee.addListener('SHOW_COMMENTS_LOAD', ()=>{
            this.setState({
                commentContent: commentStore.content
            })
        })

        let serializedCredentials=localStorage.getItem("Credentials");
        if(serializedCredentials!=null){
            let credentials=JSON.parse(serializedCredentials);
            this.setState({
                token:credentials.token
            })
        }
       
    }

    addSerie(){
        toastr.info("Seen show added");
        toastr.options.closeMethod = 'fadeOut';
        toastr.options.closeDuration = 300;
        toastr.options.closeEasing = 'swing';
        showStore.postAddShow(this.state.id, this.state.token);
    }

    addToAnalyse() {
        this.props.ee.emit('update');
        toastr.info("Show added to be analysed");
        toastr.options.closeMethod = 'fadeOut';
        toastr.options.closeDuration = 300;
        toastr.options.closeEasing = 'swing';
        this.props.onAddAnalyse(this.props.serieId);     
    }

    displayReviews(){
        reviewStore.getShowReviews(this.state.id);
        this.setState({
            seeReviews: !this.state.seeReviews
        })
    }

    displayComments(){
        commentStore.getShowComments(this.state.id);
        this.setState({
            seeComments: !this.state.seeComments
        })
    }

    handleOnReviewAdded(){
        this.setState({
            seeReviews:false
        })
    }

    handleOnCommentAdded(){
        
        this.setState({
            seeComments:false
        }, console.log("Comment added"))
    }

    render(){

        return (
                <div className="col-md-4">
                    <div className="card mb-4 box-shadow">
                        <img className="card-img-top" src={this.props.serieImage} alt="?" height="400px"/>
                        <div className="card-body">
                            <p className="card-text">{this.props.serieDescription}</p>
                            <div className="d-flex justify-content-between align-items-center">
                                <div className="btn-group">
                                    <button type="button" className="btn btn-sm btn-outline-secondary" onClick={this.addSerie}>+ Seen List</button>
                                    <button type="button" className="btn btn-sm btn-outline-secondary" onClick={this.addToAnalyse}>+ Analyse</button>
                                </div>
                                <small className="text-muted">{this.props.serieYear}</small>
                            </div>
                            <button type="button" className="btn btn-primary" data-toggle="modal" data-target={"#".concat(this.props.serieId)}>See more</button>



                            <div className="modal fade" id={this.props.serieId} tabIndex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                            <div className="modal-dialog" role="document">
                                <div className="modal-content">
                                <div className="modal-header">

                                    <div className="col-md-8">
                                        <h5 className="modal-title" id="exampleModalLongTitle">{this.props.serieName}</h5>
                                    </div>
                                    <div className="col-md-3 col-md-offset-3" align="right">
                                        <div align="right"><span>{this.props.serieImdbRating}<img src="http://hanslodge.com/images/kcMKBByXi.jpg" height="25" width="25"/></span></div>

                                    </div>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                    </button>
                                    
                                    
                                </div>
                                <div className="modal-body">
                                    <div className="border border-primary">{this.props.serieDescription}</div>
                                    <div align="left"><b>Categories: </b>
                                        {
                                            this.props.serieCategories.map((category, index) => {
                                                return (
                                                    <span key={index}>{category.name}, </span>
                                                )})}
                                    </div>
                                    <div align="left"><b>Year: </b>{this.props.serieYear}</div>
                                    <div align="left"><b>Actors: </b>{this.props.serieActors}</div>
                                    <div align="left"><b>Awards: </b>{this.props.serieAwards}</div>
                                    <div align="left"><b>Imdb votes: </b>{this.props.serieImdbVotes}</div>
                                    <div align="left"><b>Searches: </b>{this.props.serieSearches}</div>

                                     
                                     <div className="detailBox">
                                        <div className="reviewheader">
                                        <label>Reviews    </label>   
                                            <button type="button" className="fa fa-arrow-down" aria-hidden="true" onClick={this.displayReviews}></button>
                                        </div>
                                        <Reviews see={this.state.seeReviews} rating={this.state.rating} content={this.state.reviewContent} showId={this.state.id} onReviewAdded={this.handleOnReviewAdded}/>
                                    </div>


                                    
                                    <div className="detailBox">
                                        <div className="reviewheader">
                                        <label>Comments</label>
                                            <button type="button" className="fa fa-arrow-down" aria-hidden="true" onClick={this.displayComments}></button>
                                        </div>
                                        <Comments see={this.state.seeComments} content={this.state.commentContent} showId={this.state.id} onCommentAdded={this.handleOnCommentAdded}/>
                                        
                                    </div>


                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
        )
    }
}

export default SerieItem;