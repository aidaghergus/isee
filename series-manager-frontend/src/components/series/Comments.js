import React, {Component} from 'react'
import Navbar from '../workspace/Navbar';
import StarRatingComponent from 'react-star-rating-component';
import CommentStore from '../../apiserver/CommentStore';
import {EventEmitter} from 'fbemitter'


const ee = new EventEmitter()
const commentStore=new CommentStore(ee)

class Comments extends Component{

    constructor(props){
        super(props);
        this.state={
            see:props.see,
            commentToBeAdded:"",
            token:""
        }
        this.addComment=this.addComment.bind(this);
        this.handleInputChange=this.handleInputChange.bind(this);
    }

    componentDidMount(){
        let serializedCredentials=localStorage.getItem("Credentials");
        if(serializedCredentials!=null){
            let credentials=JSON.parse(serializedCredentials);
            this.setState({
                token:credentials.token
            })
        }


        ee.addListener('COMMENT_ADDED', ()=>{  
            this.setState({
                commentToBeAdded:""
            }, this.props.onCommentAdded())

        })
    }

    addComment(){
        let obj={
            id:1,
            commentDate:"",
            content:this.state.commentToBeAdded,
            user:{},
            show:{}
        }

        commentStore.postComment(this.state.token,this.props.showId,obj)

    }

    handleInputChange(e){
        this.setState({
            commentToBeAdded:e.target.value
        })
    }

    render(){
        if(this.props.see==true){
            return (
               
                <div className="actionBox">
                <ul className="commentList">
                    {
                        this.props.content.map(comment=>{
                            return(
                                <li key={comment.id} className="comment-container">
                                    <div className="commenterImage">
                                        <img src="https://www.weact.org/wp-content/uploads/2016/10/Blank-profile.png" />
                                    </div>
                                    <p className="float-left">{comment.user.username}</p><br/>
                                    <div className="commentText">
                                        <p className="">{comment.content}</p> 
                                        <span className="date sub-text">{comment.commentDate}</span>
                                    </div>
                                </li>
                            )
                        })
                    }
                </ul>
                <div className="form-group-review">
                    <div className="rows">
                        <input className="form-control" type="text" placeholder="Add a comment" value={this.state.commentToBeAdded} onChange={this.handleInputChange}/>
                        <button className="btn btn-outline-primary" onClick={this.addComment}>Add</button>

                    </div>
                </div>
                
                </div>
    
            )}
            else{
                return(<div></div>)
            }
       
    }

}

export default Comments;

