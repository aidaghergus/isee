import React, {Component} from 'react'
import db from '../../apiserver/db';
import SerieItemSimplified from '../series/SeriesItemSimplified';
import ShowStore from '../../apiserver/ShowStore';
import {EventEmitter} from 'fbemitter'
import AnalyseStore from '../../apiserver/AnalyseStore';

const ee=new EventEmitter()
const showStore=new ShowStore(ee)

class TopSeriesList extends Component{
    constructor(props){
        super(props);
        this.state={
            topSeries: [],
            temp:[]
        }
    }

    componentDidMount(){
        showStore.getTopShows()
        ee.addListener('TOP_SHOWS_LOAD', () => {
          
          let shows=new Set();
          showStore.content.map(serie => {
                shows.add(serie.id)
          })

          this.setState({
            topSeries: showStore.content,
            temp: shows
          }, this.props.handleTopSeries(shows));
        })

   
    }


    render(){
        return (
            <div>
                <div className="album py-5 bg-light">
                    <div className="container">
                    <div className="row">
                        {
                            this.state.topSeries.map(serie => {
                                return (
                                    <SerieItemSimplified key={serie.id}  
                                    serieId={serie.id}
                                    serieName={serie.name} 
                                    serieDescription={serie.description} 
                                    serieYear={serie.year} 
                                    serieImage={serie.image}
                                    serieCategories={serie.categoryList}
                                    serieComments={serie.commentList}
                                    seriesReviews={serie.reviewList}
                                    serieImdbRating={serie.imdbRating}
                                    serieImdbVotes={serie.imdbVotes}
                                    serieExtRating={serie.externalRating}
                                    serieActors={serie.actors}
                                    serieAwards={serie.awards}
                                    serieSearches={serie.searches} 
                                    />
                                )
                            })
                        }
                    </div>
                    </div>
                </div>
            
            </div>
        )
    }
}

export default TopSeriesList;