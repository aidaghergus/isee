import React , {Component} from 'react';
import { NavLink } from 'react-router-dom';
import './Navbar.css';
import toastr from 'toastr';
import Analysis from '../analysis/Analysis.js';
import {EventEmitter} from 'fbemitter'


const ee = new EventEmitter()
const ee2 = new EventEmitter()

function LoadingModal(props){
    if(props.analyse==1){
        return (<div>Aida</div>)
    }
    return null; 
}


class Navbar extends Component {

    constructor(props) {
        super(props)
        this.state = {
            nr: "",
            analyse: 0
        }

        this.handleAnalysisButton=this.handleAnalysisButton.bind(this);
        this.handleLoading=this.handleLoading.bind(this);
    }

    componentDidMount() {
        if(this.props.ee!=undefined)
            this.props.ee.addListener('update',()=>{
                this.setState(()=>({
                    nr: "https://cdn1.iconfinder.com/data/icons/color-bold-style/21/08-512.png"
                }))
            })



        ee2.addListener('ANALYSIS_IN_PROGRESS', () => {
            console.log("Event2")
        })

       
    }

    handleLoading(){

        while(this.state.analyse===1){
            toastr.error("Loading");
            toastr.options.closeMethod = 'fadeOut';
            toastr.options.closeDuration = 6000;
            toastr.options.closeEasing = 'swing';
        }
    }

    handleAnalysisButton(){

        if(this.state.nr=="" || this.props.hist==undefined){
            toastr.error("Choose shows to analyse");
            toastr.options.closeMethod = 'fadeOut';
            toastr.options.closeDuration = 300;
            toastr.options.closeEasing = 'swing';
        }
        else
        {
            console.log("DA");

            this.setState({
                analyse: 1
            }, ee2.emit('ANALYSIS_IN_PROGRESS') )

            this.props.onLoading();
          

        }

    }

    render() {

            return (
                <nav className="navbar navbar-expand-lg navbar-dark fixed-top bg-dark justify-content-between">
                    <a href="#" className="navbar-brand"><img src="https://thumbs.dreamstime.com/b/pantera-blue-eyes-colorful-illustration-graphic-background-59986293.jpg" height="40" alt=""/>   {this.props.title}</a>
                    <ul className="navbar-nav">
                        
                        <li className="nav-item">
                            <NavLink exact to='/workspace' activeClassName="active"
                                className="nav-link">Home</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink exact to='/series'  activeClassName="active"
                                className="nav-link">Series</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to='/news'  activeClassName="active"
                                className="nav-link">News</NavLink>
                        </li>
        
                        <li className="nav-item">
                        
                            <button type="button" className="btn btn-secondary btn-sm active" onClick={this.handleAnalysisButton}>
                                <img src="https://cdn1.iconfinder.com/data/icons/computers-and-development/432/seo-512.png" height="30" alt=""/>
                                <span className="badge"><img src={this.state.nr} height="20" weight="20"/></span>
                                
                                </button>

                            
                        </li>
        
                        <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="https://cdn.onlinewebfonts.com/svg/img_76927.png" height="30" alt=""/>
                            </a>
                            <div className="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown01">
                                <NavLink to='/profile' className="dropdown-item">View profile</NavLink>
                                <NavLink to='/login' className="dropdown-item">Log out</NavLink>
                            </div>
                        </li>
                        
                    </ul>
                </nav>
            )
        }
       
}

Navbar.defaultProps = {
    title: 'ISee'
}

export default Navbar;