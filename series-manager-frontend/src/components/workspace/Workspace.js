import React, {Component} from 'react'
import './Workspace.css'
import Navbar from './Navbar';
import TopSeriesList from './TopSeriesList';


class Workspace extends Component{

    constructor(){

        super();
        this.handlePrediction=this.handlePrediction.bind(this);
        this.updateSeries=this.updateSeries.bind(this);

        this.state = {
            analysedSeries: []
        }

        this.handleTopSeriesValue=this.handleTopSeriesValue.bind(this);

    }

    handlePrediction(){
        this.props.history.push({
            pathname: '/analysis',
            state: { detail: this.state.analysedSeries }
          })
    }

    updateSeries(series){
        this.setState(()=>{
            analysedSeries: series
        }, console.log("Face"))

    }

    handleTopSeriesValue(shows){
        this.setState({
            analysedSeries:shows
        }, console.log("Handle top series value"))

    }

    render(){
        return (
            <div  className="main-div-workspace">
                <Navbar/>

                <main role="main" className="container">
                <div className="jumbotron">
                    <h1>Top series</h1>
                    <p className="lead">Who is most likely to win Emmy Award?</p>
                    <a className="btn btn-lg btn-primary" href="/analysis" role="button" onClick={this.handlePrediction}>Make prediction &raquo;</a>
                </div>
                </main>

                <TopSeriesList updateSer={this.updateSeries} handleTopSeries={this.handleTopSeriesValue}/>
            </div>
        );
    }

}

export default Workspace;