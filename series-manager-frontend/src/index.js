import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/app/App';
import {BrowserRouter} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.css'
import '../node_modules/toastr/build/toastr.min.css'

ReactDOM.render((

    <BrowserRouter>
        <App />
    </BrowserRouter>


), document.getElementById('root'));
